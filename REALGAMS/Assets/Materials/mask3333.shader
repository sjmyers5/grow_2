﻿Shader "Custom/mask3333" {
    Properties {
        _MainTex ("Top (RGB)", 2D) = "white" {}
        _Alpha ("Alpha (A)", 2D) = "white" {}
        _MainTex2 ("Bottom (RGB)", 2D) = "white" {}
    }
    SubShader {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent"}
       
        ZWrite Off
       
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask RGB




        Pass {
            SetTexture[_MainTex] {
                Combine texture
            }
            SetTexture[_Alpha] {
                Combine previous, texture

            }

          //  SetTexture [_MainTex2]
			//{	
			//combine texture lerp(previous) previous

             //}         
        }
    }
}