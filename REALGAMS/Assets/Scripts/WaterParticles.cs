﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class WaterParticles : MonoBehaviour {



	public float waitTime = 3;

	public GameObject myPuddles; 


	private ParticleCollisionEvent[] collisionEvents = new ParticleCollisionEvent[16];


	public GameObject groundPuddles;

	public bool groundIsWet;

	public ParticleSystem flowingWater;


	public Transform PaintPrefab;

	private int MinSplashs = 1;
	private int MaxSplashs = 1;
	private float SplashRange = 1f;

	private float MinScale = 0.25f;
	private float MaxScale = 2.5f;

	// DEBUG
	public bool mDrawDebug;
	public Vector3 mHitPoint;
	public List<Ray> mRaysDebug = new List<Ray>();


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		if (Input.GetMouseButtonDown(0))
		{
			// Raycast
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, Mathf.Infinity))
			{
				// Paint!
				// Step back a little for a better effect (that's what "normal * x" is for)
				Paint(hit.point + hit.normal * (SplashRange / 4f));
			}
		}
			
	}


	public void Paint(Vector3 location)
	{
		//DEBUG
		mHitPoint = location;
		mRaysDebug.Clear();
		mDrawDebug = true;

		int n = -1;

		int drops = Random.Range(MinSplashs, MaxSplashs);
		RaycastHit hit;

		// Generate multiple decals in once
		while (n <= drops)
		{
			n++;

			// Get a random direction (beween -n and n for each vector component)
			var fwd = transform.TransformDirection(Random.onUnitSphere * SplashRange);

			mRaysDebug.Add(new Ray(location, fwd));

			// Raycast around the position to splash everwhere we can
			if (Physics.Raycast(location, fwd, out hit, SplashRange))
			{
				// Create a splash if we found a surface
				var paintSplatter = GameObject.Instantiate(PaintPrefab, hit.point,

					// Rotation from the original sprite to the normal
					// Prefab are currently oriented to z+ so we use the opposite
					Quaternion.FromToRotation(Vector3.back, hit.normal)
				) as Transform;

				// Random scale
				var scaler = Random.Range(MinScale, MaxScale);

				paintSplatter.localScale = new Vector3(
					paintSplatter.localScale.x * scaler,
					paintSplatter.localScale.y * scaler,
					paintSplatter.localScale.z
				);

				// Random rotation effect
				//var rater = Random.Range(0, 359);
				//paintSplatter.transform.RotateAround(hit.point, hit.normal, rater);

				paintSplatter.transform.rotation = new Quaternion(0, 0, 0, 1);


				// TODO: What do we do here? We kill them after some sec?
				Destroy(paintSplatter.gameObject, 3);
			}

		}
	}





	/*void OnParticleCollision(GameObject other)
	{

		Rigidbody partRb = other.GetComponent<Rigidbody>();

		if(partRb)
		{

			Debug.Log("I did it, mom!");


		}

		/*int safeLength = waterPart.GetSafeCollisionEventSize();
		if(collisionEvents.Length < safeLength)
		{

			collisionEvents = new ParticleCollisionEvent[safeLength];


		}*/




	void OnParticleCollision(GameObject other)
	{
		int safeLength = GetComponent<ParticleSystem>().GetSafeCollisionEventSize();
		if (collisionEvents.Length < safeLength)
			collisionEvents = new ParticleCollisionEvent[safeLength];
		int numCollisionEvents = GetComponent<ParticleSystem>().GetCollisionEvents(other, collisionEvents);
		int i = 0;
		while (i < numCollisionEvents) {
			Vector3 collisionHitLoc = collisionEvents[i].intersection;
            Vector3 normal = collisionEvents[i].normal;
            myPuddles = Instantiate(groundPuddles, collisionHitLoc+normal/100.0f, Quaternion.FromToRotation(Vector3.up, normal)) as GameObject;
            i++;

			Debug.Log("particles hitting yayyyy");

			StartCoroutine(hidePuddlesDelay(myPuddles));

			//groundPuddles.SetActive(false);
		}
	
			
	}

	IEnumerator hidePuddlesDelay(GameObject groundPuddles)
	{


		yield return new WaitForSeconds(waitTime);
		Debug.Log("I'm doooone");

		groundPuddles.SetActive(false);

	}



		// Raycast
	//	Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

	//	RaycastHit hit;

	//	if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	//	{
			// Paint!
			// Step back a little for a better effect (that's what "normal * x" is for)
	//		Paint(hit.point + hit.normal * (SplashRange / 4f));
	//	}

	//	Ray ray new 





}
