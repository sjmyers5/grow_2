using UnityEngine;
using System.Collections;

public class mychar : MonoBehaviour {

	// defining the rigidbody
	public Rigidbody rby;
public float extraNum;
	// defining movement
	public Vector3 moveVector;

	// remembers which direction the character is facing when velocity is 0/when you last moved
	public Vector3 facingVector;

	// we wanna point toward moveVector but REAL SLOWWWW so this will grow up to be moveVector someday but slowwww
	public Vector3 currentDirection;

	public float directionDistance;

	// obv speed, can be changed
	public float speed = 1;

	// defining rotation
	public Vector3 cubeRot;

	public bool isMoving;

	// controls the 3D model
	public Transform character;

	public Vector3 characterPos;

	public GameObject cam;

	//public Animator anim;

	public Vector3 fallingVector;

	public bool canJump;

	public float gravityCounter;

	public float jumpSpeed;

	public Animator anim;

	public bool FLRjump;

	public ParticleSystem flowingWater;

	public bool isShooting;

	//public ParticleCollisionEvent[] collisionEvents;




	//public SmoothFollow camScr;

	// Use this for initialization
	void Start ()
	{

		character.parent = null;

		//flowingWater.Stop();



	
	}
	
	// Update is called once per frame
	// this is where the stuff happens
    void FixedUpdate()
    {

        // >_>
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

		//this vvv is instead of a boolean! :O
		isMoving = Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0;

		/*if(!isShooting)
		{

			isShooting = false;

		}*/
			
		Movement();

		Rotation();

		Character();

		Jumping();

		FlowerBounce();

		shootGun();

		//is falling?
		if(rby.velocity.y <= 0.0f && !canJump)
		{

			isThisTheGround();

		}

	}

	// lets gooooooo
	void Movement()
	{

        float AccelRate = 100.0f;
        float MaxSpeed = 5.0f;
        float Gravity = 9.81f;

		// setting move vector equal to your input (WASD)
        Vector3 inputvector = Input.GetAxis("Horizontal") * transform.right + Input.GetAxis("Vertical") * transform.forward;
        if (inputvector.magnitude > 1.0f) {
            inputvector.Normalize();
        }

        rby.AddForce(inputvector * AccelRate, ForceMode.Acceleration);

        Vector3 speed2d = rby.velocity;
        speed2d.y = 0.0f;

        rby.AddForce(-speed2d.normalized * AccelRate * (speed2d.magnitude / MaxSpeed), ForceMode.Acceleration);

        rby.AddForce(Gravity*Vector3.down, ForceMode.Acceleration);
        moveVector = inputvector * speed + fallingVector;
            
        // setting the object's velocity to that variable (moveVector, in this case)
		//rby.velocity = moveVector;

		if(isMoving)
		{

			facingVector = moveVector;

			anim.SetBool("isMoving", true);

		} else {

			anim.SetBool("isMoving", false);

		}

	}


	void Rotation()
	{

		// setting the rotation to be equal to what the current rotation is. every frame checks what the current rotation is
		// based on the mouse
		cubeRot = this.transform.localEulerAngles;

		// adding rotation from the mouse
		cubeRot.y += Input.GetAxis("Mouse X") * 3;

		// setting the object's rotation
		this.transform.localEulerAngles = cubeRot;


	}

	void Character()
	{
		//Debug.Log(directionDistance);

		currentDirection = moveVector;

		currentDirection.y = 0;

		characterPos = this.transform.position;

		//characterPos.y -= .5f;
		//characterPos.x += 0.366f;
		//characterPos.z += 0.164f;

		character.position = characterPos;

		if(isMoving)
		{
			//(character.rotation.eulerAngles - Quaternion.LookRotation(moveVector).eulerAngles).sqrMagnitude;

			directionDistance = Mathf.Sqrt((character.rotation.eulerAngles - Quaternion.LookRotation(currentDirection).eulerAngles).sqrMagnitude);

			directionDistance = (directionDistance * 5) / 180;

			directionDistance = Mathf.Clamp(directionDistance, 1, 5);

			character.rotation = Quaternion.Lerp(character.rotation, Quaternion.LookRotation(currentDirection), (Time.deltaTime * 10) / directionDistance);




		}
			

	}

	void Jumping()
	{
		if(Input.GetAxis("Jump") == 1 && canJump == true)
		{


			jumpSpeed = 5.0f;
            Vector3 velocity2d = rby.velocity;
            velocity2d.y = 0.0f;
            rby.velocity = velocity2d;
            rby.AddForce(jumpSpeed * Vector3.up, ForceMode.VelocityChange);

			gravityCounter = 0;

			canJump = false;

			anim.SetBool("isJumping", true);

		}


		if(!canJump && gravityCounter < 50)
		{
			//gravityCounter += Time.deltaTime * 30;

		}

	}


	void FlowerBounce()
	{

		if(FLRjump)
		{

			if(canJump)
			{

				jumpSpeed = 30;

				gravityCounter = 0;

				canJump = false;

				anim.SetBool("isJumping", true);

			}

			fallingVector.y = jumpSpeed - gravityCounter;


			if(!canJump && gravityCounter < 50)
			{
				gravityCounter += Time.deltaTime * 30;

			}

		}
	}

	void OnCollisionEnter(Collision col)
	{

		if(col.gameObject.tag == "Ground")
		{

			/*canJump = true;
			gravityCounter = 5;
			jumpSpeed = 0;*/

		}

		if(col.gameObject.tag == "JumpFlower")
		{

			FLRjump = true;

		}

	}

	void isThisTheGround()
	{

		RaycastHit hit;

		if(Physics.Raycast(transform.position, Vector3.down, out hit, 0.75f))
		{
            Debug.DrawRay(transform.position, Vector3.down * 1f, Color.red);

			if(hit.collider.tag == "Ground")
			{

				landed();


			}

			if(hit.collider.tag == "JumpFlower")
			{

				canJump = true;


			}

			//Debug.Log(hit.collider.gameObject);

		}



	}

	void landed()
	{

		canJump = true;

		gravityCounter = 3;

		jumpSpeed = 0;

		anim.SetBool("isJumping", false);

		FLRjump = false;

	}

	void shootGun()
	{

		if(Input.GetButton("Fire1") && !isShooting)
		{

			isShooting = true;

			flowingWater.Play();

			Debug.DrawRay(transform.position, Vector3.forward * 5f, Color.blue);

		}

		if(!Input.GetButton("Fire1") && isShooting)
		{

			isShooting = false;

			flowingWater.Stop();

			//Debug.DrawRay(transform.position, Vector3.forward * 5f, Color.blue);

		}

	
	
	}
		
	void characterToGroundTexture()
	{



		//this.transform.position(Vector3 



	}




}
